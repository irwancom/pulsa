<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('login_model');
        $this->load->model('profil_model');
        $this->load->model('produk_model');
        $this->load->model('order_model');
        $this->load->model('transaksi_model');
        $this->load->model('registrasi_model');

        $this->load->helper(array('form', 'url'));
        $this->load->library('someclass');
    }

    public function index() {
        $data = $this->someclass->smssend('628986002287','INI ISI SMS');
        echo $data;
    }

    public function login() {
        header('Content-Type: application/json');
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $data = $this->login_model->login($this->input->post('hp'), $this->input->post('pin'), $this->input->post('idAndroid'));
            if (!empty($data)) {
                $data = array(
                    'status' => 1,
                    'msg' => 'ok',
                    'data' => $data
                );
                echo json_encode($data);
            } else {
                $data = array(
                    'status' => 0,
                    'msg' => 'gagal',
                    'data' => 'DATA TIDAK DI TEMUKAN'
                );
                echo json_encode($data);
            }
        } else {
            $data = array(
                'status' => 99,
                'msg' => 'error',
                'data' => 'DATA TIDAK DI TEMUKAN'
            );
            echo json_encode($data);
        }
    }
    
    public function registrasi() {
        header('Content-Type: application/json');
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $data = $this->registrasi_model->registrasi(
                    $this->input->post('hp'), 
                    $this->input->post('nama'), 
                    $this->input->post('nama_toko'), 
                    $this->input->post('alamat'), 
                    $this->input->post('status'), 
                    $this->input->post('saldo'), 
                    $this->input->post('id_upline'), 
                    $this->input->post('idAndroid'));
            if (!empty($data)) {
                $data = array(
                    'registrasiResponse' => $data
                );
                echo json_encode($data);
            }else {
                $data = array(
                    'status' => 00,
                    'msg' => 'gagal'
                );
                echo json_encode($data);
            }
        }
    }

    public function profile() {
        header('Content-Type: application/json');
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $data = $this->profil_model->profil($this->input->post('idMember'), $this->input->post('idAndroid'), $this->input->post('keyCode'));
            if (!empty($data)) {
                $data = array(
                    'status' => 1,
                    'msg' => 'ok',
                    'data' => $data
                );
                echo json_encode($data);
            } else {
                $data = array(
                    'status' => 00,
                    'msg' => 'gagal',
                    'data' => 'AKUN TELAH DI GUNAKAN DI HP LAIN'
                );
                echo json_encode($data);
            }
        } else {
            $data = array(
                'status' => 99,
                'msg' => 'fild',
                'data' => 'DATA TIDAK DI TEMUKAN'
            );
            echo json_encode($data);
        }
    }

    public function ping_longlang() {
        header('Content-Type: application/json');
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $data = $this->profil_model->pinglonglang($this->input->post('idMember'), $this->input->post('longlang'));
            if (!empty($data)) {
                $data = array(
                    'status' => 1,
                    'msg' => 'ok',
                    'data' => 'BERHASIL DI TAMBAH',
                    'dataPhone' => $data
                );
                echo json_encode($data);
            } else {
                $data = array(
                    'status' => 00,
                    'msg' => 'gagal',
                    'data' => 'DATA SUDAH ADA'
                );
                echo json_encode($data);
            }
        } else {
            $data = array(
                'status' => 99,
                'msg' => 'fild',
                'data' => 'DATA TIDAK DI TEMUKAN'
            );
            echo json_encode($data);
        }
    }

    public function phonebook_all() {
        header('Content-Type: application/json');
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $data = $this->profil_model->profil($this->input->post('idMember'), $this->input->post('idAndroid'), $this->input->post('keyCode'));
            if (!empty($data)) {
                $dataPh = $this->input->post('dataPh');
                $dataPh = json_decode($dataPh);
                if (!empty($dataPh)) {
                    foreach ($dataPh as $ddt) {
                        $data = $this->profil_model->phonebook($this->input->post('idMember'), $ddt->name, $ddt->phone);
                    }
                }
                if (!empty($data)) {
                    $data = array(
                        'status' => 1,
                        'msg' => 'ok',
                        'data' => 'BERHASIL DI TAMBAH'
                    );
                    echo json_encode($data);
                } else {
                    $data = array(
                        'status' => 00,
                        'msg' => 'gagal',
                        'data' => 'DATA SUDAH ADA'
                    );
                    echo json_encode($data);
                }
            } else {
                $data = array(
                    'status' => 00,
                    'msg' => 'gagal',
                    'data' => 'DATA SUDAH ADA'
                );
                echo json_encode($data);
            }
        } else {
            $data = array(
                'status' => 99,
                'msg' => 'fild',
                'data' => 'DATA TIDAK DI TEMUKAN'
            );
            echo json_encode($data);
        }
    }

    public function getProduk() {
        header('Content-Type: application/json');
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $data = $this->profil_model->profil($this->input->post('idMember'), $this->input->post('idAndroid'), $this->input->post('keyCode'));
            if (!empty($data)) {
                $data = $this->produk_model->cekProduk($this->input->post('idMember'), $this->input->post('hp'), $this->input->post('idAndroid'), $this->input->post('keyCode'));
                $data = array(
                    'status' => 1,
                    'msg' => 'ok',
                    'data' => $data
                );
                echo json_encode($data);
            } else {
                $data = array(
                    'status' => 00,
                    'msg' => 'gagal',
                    'data' => 'AKUN TELAH DI GUNAKAN DI HP LAIN'
                );
                echo json_encode($data);
            }
        } else {
            $data = array(
                'status' => 99,
                'msg' => 'fild',
                'data' => 'DATA TIDAK DI TEMUKAN'
            );
            echo json_encode($data);
        }
    }

    public function getProdukPln() {
        header('Content-Type: application/json');
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $data = $this->profil_model->profil($this->input->post('idMember'), $this->input->post('idAndroid'), $this->input->post('keyCode'));
            if (!empty($data)) {
                $data = $this->produk_model->getPLN();
                $data = array(
                    'status' => 1,
                    'msg' => 'ok',
                    'data' => $data
                );
                echo json_encode($data);
            } else {
                $data = array(
                    'status' => 00,
                    'msg' => 'gagal',
                    'data' => 'AKUN TELAH DI GUNAKAN DI HP LAIN'
                );
                echo json_encode($data);
            }
        } else {
            $data = array(
                'status' => 99,
                'msg' => 'fild',
                'data' => 'DATA TIDAK DI TEMUKAN'
            );
            echo json_encode($data);
        }
    }

    public function orderPulsa() {
        header('Content-Type: application/json');
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $dataP = $this->order_model->cekProduk($this->input->post('kode_produk'));
            $data = $this->profil_model->profil($this->input->post('idMember'), $this->input->post('idAndroid'), $this->input->post('keyCode'), $this->input->post('pin'));
            if (!empty($data)) {
                $dataOrder = array(
                    'id_member' => $data[0]->idMember,
                    'tgl' => date('Y-m-d H:i:s'),
                    'nama' => $data[0]->nama,
                    'hp' => $data[0]->hp,
                    'produk' => $this->input->post('kode_produk'),
                    'tujuan' => $this->input->post('hp'),
                    'jml' => $dataP[0]->priceSell,
                    'saldo_awal' => $data[0]->saldo
                );

                $dataSms = array(
                    'id_member' => $data[0]->idMember,
                    'waktu' => date('Y-m-d H:i:s'),
                    'hp' => $this->input->post('hp'),
                    'longlang' => $this->input->post('longlang'),
                    'prov' => strtoupper($this->input->post('prov')),
                    'kokab' => strtoupper($this->input->post('kokab')),
                    'kec' => strtoupper($this->input->post('kec')),
                    'kel' => strtoupper($this->input->post('kel')),
                    'alamat' => strtoupper($this->input->post('alamat'))
                );
                $data = $this->order_model->orderPulsa($dataOrder, $dataSms);
                if (!empty($data['dataOrder'])) {
                    $this->someclass->javah2h('ISI', $this->input->post('kode_produk'), $this->input->post('hp'), $data['idTrx']);
                }
                $data = array(
                    'status' => 1,
                    'msg' => 'ok',
                    'data' => $data
                );
                echo json_encode($data);
            } else {
                $data = array(
                    'status' => 00,
                    'msg' => 'gagal',
                    'data' => 'AKUN TELAH DI GUNAKAN DI HP LAIN'
                );
                echo json_encode($data);
            }
        } else {
            $data = array(
                'status' => 99,
                'msg' => 'fild',
                'data' => 'DATA TIDAK DI TEMUKAN'
            );
            echo json_encode($data);
        }
    }

    public function orderPlnToken() {
        header('Content-Type: application/json');
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $dataP = $this->order_model->cekProduk($this->input->post('kode_produk'));
            $data = $this->profil_model->profil($this->input->post('idMember'), $this->input->post('idAndroid'), $this->input->post('keyCode'), $this->input->post('pin'));
            if (!empty($data)) {
                $dataOrder = array(
                    'id_member' => $data[0]->idMember,
                    'tgl' => date('Y-m-d H:i:s'),
                    'nama' => $data[0]->nama,
                    'hp' => $data[0]->hp,
                    'produk' => $this->input->post('kode_produk'),
                    'tujuan' => $this->input->post('nopln'),
                    'tujuan2' => $this->input->post('hp'),
                    'jml' => $dataP[0]->priceSell,
                    'saldo_awal' => $data[0]->saldo
                );

                $dataSms = array(
                    'id_member' => $data[0]->idMember,
                    'waktu' => date('Y-m-d H:i:s'),
                    'hp' => $this->input->post('hp'),
                    'longlang' => $this->input->post('longlang'),
                    'prov' => $this->input->post('prov'),
                    'kokab' => $this->input->post('kokab'),
                    'kec' => $this->input->post('kec'),
                    'kel' => $this->input->post('kel'),
                    'alamat' => $this->input->post('alamat')
                );
                $data = $this->order_model->orderPulsa($dataOrder, $dataSms);
                if (!empty($data)) {
                    $this->someclass->javah2h('PLN', $this->input->post('kode_produk'), $this->input->post('nopln'), $data['idTrx']);
                }
                $data = array(
                    'status' => 1,
                    'msg' => 'ok',
                    'data' => $data
                );
                echo json_encode($data);
            } else {
                $data = array(
                    'status' => 00,
                    'msg' => 'gagal',
                    'data' => 'AKUN TELAH DI GUNAKAN DI HP LAIN'
                );
                echo json_encode($data);
            }
        } else {
            $data = array(
                'status' => 99,
                'msg' => 'fild',
                'data' => 'DATA TIDAK DI TEMUKAN'
            );
            echo json_encode($data);
        }
    }

    public function historiTransaksi() {
        header('Content-Type: application/json');
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $data = $this->transaksi_model->transaksi($this->input->post('page'), 10, $this->input->post('idMember'));
            if (!empty($data)) {
                $data = array(
                    'status' => 1,
                    'msg' => 'ok',
                    'data' => $data
                );
                echo json_encode($data);
            } else {
                $data = array(
                    'status' => 0,
                    'msg' => 'gagal',
                    'data' => 'DATA TIDAK DI TEMUKAN'
                );
                echo json_encode($data);
            }
        } else {
            $data = array(
                'status' => 99,
                'msg' => 'fild',
                'data' => 'DATA TIDAK DI TEMUKAN'
            );
            echo json_encode($data);
        }
    }

    public function transferSaldo() {
        header('Content-Type: application/json');
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $data = $this->order_model->transferSaldo($this->input->post('idMember'), $this->input->post('pin'), $this->input->post('keyCode'), $this->input->post('idMemberTo'), $this->input->post('salDo'),$this->input->post('tyPe'));
            if (!empty($data)) {
                $data = array(
                    'status' => 1,
                    'msg' => 'ok',
                    'data' => $data
                );
                echo json_encode($data);
            } else {
                $data = array(
                    'status' => 0,
                    'msg' => 'gagal',
                    'data' => 'DATA TIDAK DI TEMUKAN'
                );
                echo json_encode($data);
            }
        } else {
            $data = array(
                'status' => 99,
                'msg' => 'fild',
                'data' => 'DATA TIDAK DI TEMUKAN'
            );
            echo json_encode($data);
        }
    }
    
    public function do_upload() {
        header('Content-Type: application/json');
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $config['upload_path'] = './uploads/';
            $config['allowed_types'] = 'gif|jpg|png';
//        $config['max_size'] = 100;
//        $config['max_width'] = 1024;
//        $config['max_height'] = 768;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('userfile')) {
                $error = array('error' => $this->upload->display_errors());

                echo 0;
            } else {
                $data = array('upload_data' => $this->upload->data());
                $p = $this->profil_model->uploadFile(
                        $this->input->post('idMember'), $this->upload->data('file_name'), $this->input->post('idAndroid'), $this->input->post('keyCode')
                );
//            echo $this->upload->data('file_name');
                if (!empty($p)) {
                    $data = array(
                        'status' => 1,
                        'msg' => 'ok',
                        'data' => 'BERHASIL DI TAMBAH',
                        'dataFile' => $data
                    );
                    echo json_encode($data);
                } else {
                    unlink($config['upload_path'] . $this->upload->data('file_name'));
                    $data = array(
                        'status' => 00,
                        'msg' => 'gagal',
                        'data' => 'ADA KESALAHAN DI PROSES UPLOAD'
                    );
                    echo json_encode($data);
                }
            }
        } else {
            $data = array(
                'status' => 99,
                'msg' => 'fild',
                'data' => 'DATA TIDAK DI TEMUKAN'
            );
            echo json_encode($data);
        }
    }

}
