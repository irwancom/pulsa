<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Cron extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('cron_model');
        $this->load->model('order_model');
        $this->load->library('someclass');
    }

    public function index() {
        $data = $this->someclass->javah2h('CEKHARGA', 'PULSA');

        if (!empty($data['message'])) {
            foreach ($data['message'] as $d) {
                $this->cron_model->insertUpdateProce($d['provider'], $d['provider_sub'], $d['operator'], $d['operator_sub'], $d['code'], $d['status'], $d['description'], $d['price']);
            }
        }
    }

    public function callBack() {
        if ($_SERVER['REMOTE_ADDR'] == '172.104.161.223') { // memastikan data terikirim dari server javah2h
            $data = $this->input->post('content');
            $data = json_decode($data);
            $this->order_model->orderPulsaUpdate($data);
            if (!empty($data->idcust)) {
                $dd = $this->order_model->cekOrder($data->trxid_api);
                $this->someclass->smssend($dd[0]->tujuan2, 'SELAMAT! Berikut no token PLN '.$data->sn.' untuk ip pelanggan '.$data->trxid_api);
            }
        }
    }

}
