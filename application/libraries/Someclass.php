<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Someclass {

    public function some_method() {
        echo 'xxx';
    }

    function javah2h($type = '', $var1 = '', $var2 = '', $var3 = '') {
        $url = 'https://javah2h.com/api/connect/';

        $header = array(
            'h2h-userid: H1320',
            'h2h-key: f58b2998f606e3cb931baaac2f994035', // lihat hasil autogenerate di member area
            'h2h-secret: f35a2e433ce35740f346e7b0d5ba2c5cb52e7b531d77d0efb9a7d59da7f4b0db', // lihat hasil autogenerate di member area
        );

        if ($type == 'CEKHARGA') {
            $data = array(
                'inquiry' => 'HARGA', // konstan
                'code' => $var1, // pilihan: pln, pulsa, game
            );
        } elseif ($type == 'DEPOSIT') {
            $data = array(
                'inquiry' => 'D', // konstan
                'bank' => $var1, // bank tersedia: bca, bni, mandiri, bri, muamalat
                'nominal' => $var2, // jumlah request
            );
        } elseif ($type == 'SALDO') {
            $data = array(
                'inquiry' => 'S', // konstan
            );
        } elseif ($type == 'STATUSTRX') {
            $data = array(
                'inquiry' => 'STATUS', // konstan
                'trxid_api' => $var1, // Trxid atau Reffid dari sisi client saat transaksi pengisian
            );
        } elseif ($type == 'ISI') {
            $data = array(
                'inquiry' => 'I', // konstan
                'code' => $var1, // kode produk
                'phone' => $var2, // nohp pembeli
                'trxid_api' => $var3, // Trxid / Reffid dari sisi client
            );
        } elseif ($type == 'PLN') {
            $data = array(
                'inquiry' => 'PLN', // konstan
                'code' => $var1, // kode produk
                'idcust' => $var2, // nomor meter atau id pln
                'trxid_api' => $var3, // Trxid / Reffid dari sisi client
            );
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $result = curl_exec($ch);

        $data = json_decode($result, true);
        return $data;
    }

    function smssend($number = '', $message = '') {
        $curlHandle = curl_init();
        $url = "http://sms255.xyz/sms/smsreguler.php?username=" . urlencode("irwancom") . "&key=fab702b290e19f921cf541afbd38112a&number=" . $number . "&message=" . urlencode($message);
        curl_setopt($curlHandle, CURLOPT_URL, $url);
        curl_setopt($curlHandle, CURLOPT_HEADER, 0);
        curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curlHandle, CURLOPT_TIMEOUT, 120);
        $hasil = curl_exec($curlHandle);
        curl_close($curlHandle);
        return $hasil;
    }

}
