<?php

class Cron_model extends CI_Model {

    public function __construct() {
        parent::__construct();

        $this->load->database();
    }

    public function insertUpdateProce($provider = '', $provider_sub = '', $operator = '', $operator_sub = '', $code = '', $status = '', $description = '', $price = '') {
        $query = $this->db->get_where('produk', array('code' => $code))->result();
//        print_r($query[0]->idProduk);
//        exit;
        if ($provider == 'PLN') {
            $priceUp = '350';
            $priceSell = ceil($priceUp + $price);
        } else {
            $priceUp = '0.02';
            $priceSell = ceil(($price * 0.02) + $price);
        }
        if (!empty($query)) {
            $data = array(
                'provider' => $provider,
                'provider_sub' => $provider_sub,
                'operator' => $operator,
                'operator_sub' => $operator_sub,
                'code' => $code,
                'description' => $description,
                'price' => $price,
                'priceSell' => $priceSell,
                'status' => $status,
                'udate' => date('Y-m-d H:i:s')
            );
            $this->db->update('produk', $data, array('idProduk' => $query[0]->idProduk));
        } else {

            $data = array(
                'provider' => $provider,
                'provider_sub' => $provider_sub,
                'operator' => $operator,
                'operator_sub' => $operator_sub,
                'code' => $code,
                'description' => $description,
                'priceUp' => $priceUp,
                'price' => $price,
                'priceSell' => $priceSell,
                'status' => $status,
                'udate' => date('Y-m-d H:i:s')
            );
            $this->db->insert('produk', $data);
        }
    }

}
