<?php

class Produk_model extends CI_Model {

    public function __construct() {
        parent::__construct();

        $this->load->database();
    }

    public function cekProduk($id_member = '', $nohp = '', $app_id = '', $keyCode = '') {

        $nohp = substr($nohp, 0, 4);
        $this->db->join('kartu as b', 'b.id_kartu = a.id_kartu');
        $this->db->where('a.prefix', $nohp);
        $q = $this->db->get('hlr as a')->result();

        $sl = array(
            'provider', 'provider_sub', 'operator'
        );
        $this->db->select($sl);
        $this->db->where('provider', $q[0]->nama);
        $this->db->group_by('provider_sub');
        $this->db->order_by('priceSell', 'ASC');
        $q2 = $this->db->get('produk', 10)->result();

        if (!empty($q2)) {
            foreach ($q2 as $qq2) {
                $sl = array(
                    'idProduk', 'provider', 'provider_sub', 'operator', 'code', 'description', 'status', 'priceSell', 'udate'
                );
                $q1[] = $this->db->select($sl)->where('provider', $qq2->provider)->where('provider_sub', $qq2->provider_sub)->get('produk')->result();
            }
        }

        $i = 0;
        foreach ($q2 as $qq2) {
            $dataProduk[] = array(
                'paketProduk' => $qq2,
                'produkHarga' => $q1[$i++]
            );
        }

        return $dataProduk;
    }

    public function getPLN() {
        $q = $this->db->select($sl)->where('provider', 'PLN')->get('produk')->result();
        return $q;
    }

}
