<?php

class Login_model extends CI_Model {

    public function __construct() {
        parent::__construct();

        $this->load->database();
    }

    public function login($hp = '', $pin = '', $app_id = '') {
        $sl = array(
            'id_member',
            'tgl_register',
            'nama',
            'saldo',
            'alamat'
            
        );
        $this->db->select($sl);
        $this->db->where('hp', $hp);
        $this->db->where('pin', $pin);
        $this->db->where('idAndroid', $app_id);
        $a = $this->db->get('member')->result();
        $wkt = date('Y-m-d H:i:s');

        $keyCode = md5(time() . $hp . $pin);
        $this->db->set('keyCode', $keyCode);
        $this->db->set('tgl_login', $wkt);
        $this->db->where('id_member', $a[0]->id_member);
        $this->db->update('member');
        if (!empty($a)) {
            $return = array('dataMember' => $a, 'keyCode' => array('keyCode' =>$keyCode ));
            return $return;
        } else {
            return 0;
        }
    }

}
