<?php

class Profil_model extends CI_Model {

    public function __construct() {
        parent::__construct();

        $this->load->database();
    }

    public function profil($idMember = '', $app_id = '', $keyCode = '', $pin = '') {
        $sl = array(
            'id_member as idMember',
            'nama',
            'hp',
            'saldo'
        );
        $this->db->select($sl);
        $this->db->where('id_member', $idMember);
        $this->db->where('idAndroid', $app_id);
        $this->db->where('keyCode', $keyCode);
        if (!empty($pin)) {
            $this->db->where('pin', $pin);
        }
        $this->db->order_by('id_member', 'DESC');
        $q = $this->db->get('member', 1)->result();

        return $q;
    }

    public function phonebook($id_member = '', $name = '', $no_hp = '') {
        $q = $this->db->select('*')->where('no_hp', $no_hp)->get('member_phonebook')->result();
        if (!empty($q)) {
            $this->db->set('waktu', date('Y-m-d H:i:s'));
            $this->db->set('name', $name);
            $this->db->set('no_hp', $no_hp);
            $this->db->where('no_hp', $no_hp);
            $this->db->update('member_phonebook');

            return array('msg' => 'BERHASIL DI PERBAHARUI');
        } else {
            $data = array(
                'id_member' => $id_member,
                'waktu' => date('Y-m-d H:i:s'),
                'name' => $name,
                'no_hp' => $no_hp
            );

            $this->db->insert('member_phonebook', $data);

            return $data;
        }
    }

    public function pinglonglang($id_member = '', $longlang = '') {
        $data = array(
            'id_member' => $id_member,
            'longlang' => $longlang,
            'waktu' => date('Y-m-d H:i:s')
        );

        $this->db->insert('member_pinglonglang', $data);

        return $data;
    }
    
    public function uploadFile($idMember = '', $file = '', $app_id = '', $keyCode = '') {
        $this->db->where('id_member', $idMember);
        $this->db->where('idAndroid', $app_id);
        $this->db->where('keyCode', $keyCode);
        $this->db->order_by('id_member', 'DESC');
        $q = $this->db->get('member', 1)->result();

        if (!empty($q)) {
            $data = array(
                'id_member' => $idMember,
                'waktu' => date('Y-m-d H:i:s'),
                'fileImage' => $file
            );

            $this->db->insert('member_file', $data);
            return 1;
        } else {
            return 0;
        }
    }

}
