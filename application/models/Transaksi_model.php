<?php

class Transaksi_model extends CI_Model {

    public function __construct() {
        parent::__construct();

        $this->load->database();
    }

    public function transaksi($page = '', $limit = '', $idMember = '') {
        if (empty($page)) {
            $page = 1;
        }
        if (empty($limit)) {
            $limit = 10;
        }
        $page = ($page - 1) * $limit;
        $this->db->where('id_member', $idMember);
        $count = $this->db->get('transaksi')->num_rows();
        $sl = array(
            'id',
            'tgl',
            'id_member',
            'nama',
            'hp',
            'produk',
            'tujuan',
            'modal',
            'jml',
            'round(((jml-modal)/jml)*100) as margin',
            'status',
            'sn',
        );
        $this->db->select($sl);
        $this->db->where('id_member', $idMember);
        $this->db->limit(10, $page);
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get('transaksi')->result();
        $query = array(
            'total' => "$count",
            'rows' => $query
        );
        return $query;
    }
}
