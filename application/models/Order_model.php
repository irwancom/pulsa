<?php

class Order_model extends CI_Model {

    public function __construct() {
        parent::__construct();

        $this->load->database();
    }

    public function cekProduk($kode = '') {
        $query = $this->db->get_where('produk', array('code' => $kode))->result();
        return $query;
    }
    
    public function cekOrder($id = '') {
        $query = $this->db->get_where('transaksi', array('id' => $id))->result();
        return $query;
    }

    public function orderPulsa($dataOrder, $dataSms) {

        $query = $this->db->select(array('id_member', 'hp', 'saldo'))->get_where('member', array('id_member' => $dataOrder['id_member']))->result();
        if (!empty($query)) {
            if ($query[0]->saldo > $dataOrder['jml']) {
                $this->db->insert('sms_ditail', $dataSms);
                $this->db->insert('transaksi', $dataOrder);
                $idtrx = $this->db->insert_id();
                $idtrx = array(
                    'status' => 1,
                    'msg' => 'sucess',
                    'idTrx' => $idtrx,
                    'dataOrder' => array($dataSms, $dataOrder)
                );
            } else {
                $idtrx = 0;
            }
        } else {
            $idtrx = 0;
        }
        return $idtrx;
    }

    public function orderPulsaUpdate($data) {
        if (!empty($data)) {
            $sl = array(
                'b.id',
                'b.id_member',
                'b.nama',
                'b.saldo',
            );
            $this->db->select($sl);
            $this->db->join('member as b', 'b.id_member = a.id_member', 'left');
            $this->db->where('a.id', $data->trxid_api);
            $qm = $this->db->get('transaksi as a')->result();

            $this->db->set('saldo', $qm[0]->saldo - $data->price);
            $this->db->where('id', $qm[0]->id);
            $this->db->update('member');

            $this->db->set('idtrx', $data->trxid);
            $this->db->set('ket', $data->note);
            $this->db->set('sn', $data->sn);
            $this->db->set('status', $data->status);
            $this->db->set('modal', $data->price);
            $this->db->set('stok', $data->last_balance);
            $this->db->set('tgl_update', $data->date_update);
            $this->db->set('saldo_akhir', $qm[0]->saldo - $data->price);
            $this->db->where('id', $data->trxid_api);
            $this->db->update('transaksi');
        }
    }

    public function transferSaldo($idMember = '', $pin = '', $keyCode = '', $idMemberTo = '', $salDo = '', $tyPe = '') {
        $this->db->where('id_member', $idMember);
        $this->db->where('pin', $pin);
        $this->db->where('keyCode', $keyCode);
        $query = $this->db->get('member')->result();

        $queryTo = $this->db->get_where('member', array('id_member' => $idMemberTo))->result();

        if (!empty($queryTo)) {
            if (!empty($query)) {
                if ($query[0]->saldo > $salDo) {
                    if (empty($tyPe)) {
                        $data = array(
                            'id_memberSend' => $idMember,
                            'id_memberReceive' => $idMemberTo,
                            'waktuPinjam' => date('Y-m-d H:i:s'),
                            'nominal' => $salDo
                        );

                        $this->db->insert('member_hutang', $data);
                    } elseif ($tyPe == 2) {
                        $chInv = $this->db->get_where('member_hutang', array('id_memberSend' => $idMember, 'id_memberReceive' => $idMemberTo, 'status' => 0),1)->result();
                        if (!empty($chInv)) {
                            $this->db->set('waktuBayar', date('Y-m-d H:i:s'));
                            $this->db->set('status', 1);
                            $this->db->where('idhutang', $chInv[0]->idhutang);
                            $this->db->update('member_hutang');        
                            $msg = 'TERBAYAR.';
                        }else{
                            $msg = 'TIDAK ADA TAGIHAN';
                        }
                        $dataOrder = array($chInv,'msg'=>$msg);
                    }
                    if ($tyPe != 2) {
                        $dataOrder = array(
                            'id_member' => $idMember,
                            'tgl' => date('Y-m-d H:i:s'),
                            'nama' => $query[0]->nama,
                            'hp' => $query[0]->hp,
                            'jml' => $salDo,
                            'ket' => 'TRANSFER SALDO',
                            'tgl_update' => date('Y-m-d H:i:s'),
                            'saldo_awal' => $query[0]->saldo,
                            'saldo_akhir' => $query[0]->saldo - $salDo
                        );
                        $this->db->insert('transaksi', $dataOrder);

                        $this->db->set('saldo', $query[0]->saldo - $salDo);
                        $this->db->where('id', $query[0]->id);
                        $this->db->update('member');

                        $this->db->set('saldo', $queryTo[0]->saldo + $salDo);
                        $this->db->where('id', $queryTo[0]->id);
                        $this->db->update('member');
                    }

                    $var = $dataOrder;
                } else {
                    $var = '0';
                }
            } else {
                $var = '0';
            }
        } else {
            $var = '0';
        }

        return $var;
    }

}
