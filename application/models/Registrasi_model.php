<?php

class Registrasi_model extends CI_Model {

    public function __construct() {
        parent::__construct();

        $this->load->database();
    }

    public function registrasi($hp, $nama, $nama_toko, $alamat, $status, $saldo, $id_upline, $idAndroid) {
        $this->db->where('hp', $hp);
        $this->db->from('member');
        $count = $this->db->count_all_results();


        if ($count == 0) {
            $sl = array(
                'id_member'
            );
            $this->db->select($sl);
            $this->db->order_by('id', 'DESC');
            $this->db->limit(1);
            $a = $this->db->get('member')->result();

            $idMember = $a[0]->id_member;

            $prefix = substr($id_upline, 0, 2);
            $idMember = substr($idMember, 2);

            $digit = '000000';
            $hasil = $idMember + 1;
            $lenght = strlen($hasil);
            $digit = substr($digit, $lenght);
            $idMember = ($prefix . $digit . $hasil);
            $tgl_register = date('Y-m-d');

            try {
                $data = array(
                    'id_member' => $idMember,
                    'tgl_register' => $tgl_register,
                    'nama' => $nama,
                    'hp' => $hp,                   
                    'nama_toko' => $nama_toko,
                    'alamat' => $alamat,
                    'pin' => rand(100000, 999999),
                    'status' => $status,
                    'saldo' => $saldo,
                    'id_upline' => $id_upline,
                    'idAndroid' => $idAndroid
                );

                $this->db->insert('member', $data);
                $this->db->trans_complete();

                $return = array('status' => 1, 'msg' => 'ok');
                return $return;
            } catch (Exception $e) {
                // this will not catch DB related errors. But it will include them, because this is more general. 
                log_message('error: ', $e->getMessage());
                $return = array('status' => 00, 'msg' => $e->getMessage());
                return $return;
            }
        } else {
            $return = array('status' => 00, 'msg' => 'hp duplicate');
            return $return;
        }
    }

}
